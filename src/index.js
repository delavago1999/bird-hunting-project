import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import Initial from './initial';
import * as serviceWorker from './serviceWorker';
import store from './redux/store/store.redux';

import {Provider} from 'react-redux';

const RNRedux =() => {
    return(
        <Provider store={store}>
            <App/>
        </Provider>
    )
}

ReactDOM.render(<RNRedux/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
