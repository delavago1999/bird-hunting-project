import API from '../api/config';

export let getSession = async (sessionId,token) => {
    try{
        let data =  await API.get('/hunting_session/'+sessionId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllSessions = async (token)=> {
    try{
        let data =  await API.get('/hunting_session/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllSessionsForUser = async (hunterId,token)=> {
    try{
        let data =  await API.get('/hunting_session/'+hunterId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitSession = async(sessionObj, token)=>{
    try{
        let data = await API.post('/hunting_session/',{
            reserveId: sessionObj.reserveId,
            session: sessionObj.session,
            date: sessionObj.date,
            hunterId: sessionObj.hunterId
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteSession = async (sessionId,token)=> {
    try{
        let data =  await API.delete('/hunting_session/'+sessionId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateSession = async(sessionId,sessionObj, token)=>{
    try{
        let data = await API.put('/hunting_session/'+sessionId,{
            reserveId: sessionObj.reserveId,
            session: sessionObj.session,
            date: sessionObj.date,
            hunterId: sessionObj.hunterId
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}