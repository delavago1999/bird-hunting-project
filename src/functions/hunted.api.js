import API from '../api/config';

export let getHuntedForUser = async (huntedId,sessionIds,token) => {
    try{
        let data =  await API.get('/hunted/'+huntedId,{sessionIds:sessionIds},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllHunted = async (token)=> {
    try{
        let data =  await API.get('/hunted/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitHunted = async(huntedObj, token)=>{
    try{
        let data = await API.post('/hunted/',{
            birdId: huntedObj.birdId,
            sessionId: huntedObj.sessionId,
            amount: huntedObj.amount
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteHunted = async (huntedId,token)=> {
    try{
        let data =  await API.delete('/hunted/'+huntedId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateHunted = async(huntedId,huntedObj, token)=>{
    try{
        let data = await API.put('/hunted/'+huntedId,{
            birdId: huntedObj.birdId,
            sessionId: huntedObj.sessionId,
            amount: huntedObj.amount
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}