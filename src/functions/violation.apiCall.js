import API from '../api/config';

export let getViolation = async (voilationId,token) => {
    try{
        let data =  await API.get('/violation/'+voilationId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getViolationAct = async (voilationId,token) => {
    try{
        let data =  await API.get('/violation/act/'+voilationId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllViolation = async (token)=> {
    try{
        let data =  await API.get('/violation/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllViolationAct = async (token)=> {
    try{
        let data =  await API.get('/violation/act/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitViolation = async(voilationObj, token)=>{
    try{
        let data = await API.post('/violation/',{
            type: voilationObj.type
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitViolationAct = async(voilationObj, token)=>{
    try{
        let data = await API.post('/violation/act/',{
            hunterId: voilationObj.hunterId,
            sessionId: voilationObj.sessionId,
            voilationId: voilationObj.voilationId
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteViolation = async (voilationId,token)=> {
    try{
        let data =  await API.delete('/violation/'+voilationId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteViolationAct = async (voilationId,token)=> {
    try{
        let data =  await API.delete('/violation/act/'+voilationId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateViolation = async(voilationId,voilationObj, token)=>{
    try{
        let data = await API.put('/violation/'+voilationId,{
            type: voilationObj.type
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateViolationAct = async(voilationId,voilationObj, token)=>{
    try{
        let data = await API.put('/violation/act/'+voilationId,{
            hunterId: voilationObj.hunterId,
            sessionId: voilationObj.sessionId,
            voilationId: voilationObj.voilationId
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}