import API from '../api/config';

export let getBird = async (birdId,token) => {
    try{
        let data =  await API.get('/bird/'+birdId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        console.log('API call: Bird')
        console.log(data.data.data)
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllBird = async (token)=> {
    try{
        let data =  await API.get('/bird/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitBird = async(birdObj, token)=>{
    try{
        let data = await API.post('/bird/',{
            bird_name: birdObj.bird_name,
            type: birdObj.type
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteBird = async (birdId,token)=> {
    try{
        let data =  await API.delete('/bird/'+birdId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateBird = async(birdId,birdObj, token)=>{
    try{
        let data = await API.put('/bird/'+birdId,{
            bird_name: birdObj.bird_name,
            type: birdObj.type
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}