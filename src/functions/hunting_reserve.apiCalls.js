import API from '../api/config';

export let getReserves = async (reserveId,token) => {
    try{
        let data =  await API.get('/hunting_reserve/'+reserveId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let getAllReserves = async (token)=> {
    try{
        let data =  await API.get('/hunting_reserve/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        console.log("Api call: reserves");
        console.log(data.data.data);
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let submitReserve = async(reserveObj, token)=>{
    try{
        let data = await API.post('/hunting_reserve/',{
            location: reserveObj.location,
            available: reserveObj.available
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let deleteReserve = async (reserveId,token)=> {
    try{
        let data =  await API.delete('/hunting_reserve/'+reserveId,{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let updateReserve = async(reserveId,reserveObj, token)=>{
    try{
        let data = await API.put('/hunting_reserve/'+reserveId,{
            location: reserveObj.location,
            available: reserveObj.available
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}