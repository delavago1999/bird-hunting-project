const fs = require('fs');
const path = require("path");
let jwtDecode = require('jwt-decode');

let pathToDataFile = path.resolve(__dirname,'../data/');
let file = 'data.json';

export let decodeToken = (token) => {
    try{
        let decodedData = jwtDecode(token);
        console.log(decodedData);
        return decodedData;
    }catch(err){
        console.error(err);
        throw err;
    }
}

/**
 * check if the data file exists
 * and it it doesn't ir creates it
 * true it does not exist it returns false,
 * if it does it return true
 * @returns {boolean}
 */
export let ifExist = () => {
    console.log("Checking for Data file");
    try{
        fs.access(file, (err) => {
            if(!err) return false;
        });
        return true
    }catch(err){
        console.log(err);
        throw err;
    }
}

/**
 * creates the data file if the 
 * file does not exist and returns true if it was create successfully
 * @returns {boolean}
 */
export let createDataFile = () => {
    console.log("creating data file");
    let data = {
        data: [
            {
                type: 'user',
                data: {}
            },
            {
                type: 'reserve',
                data: {}
            },
            {
                type: 'session',
                data: {}
            },
            {
                type: 'bird',
                data: {}
            },
            {
                type: 'hunted',
                data: {}
            },
            {
                type: 'violation',
                data: {
                    voilation: {},
                    violation_acts: {}
                }
            },

        ]
    }
    try{
        fs.mkdir(pathToDataFile);
        fs.writeFile(pathToDataFile+file,JSON.stringify(data));
        return ifExist();
    }catch(err){
        console.error(err);
        throw new Error("Failed to create file")
    }
}

/**
 * Find and removes a object from the array
 * and returns the new array without the object
 * @param {string} objectType 
 * @param {object} fileData 
 * @returns {object} fileData
 */
export let FindAndRemoveObject = (objectType,fileData) => {
    let index = 0;
    let count = 0;
    fileData.data.forEach(element => {
        if(element.type === objectType) index = count;
        count ++;
    });

    fileData.data.splice(index,1);

    return fileData;
}

/**
 * reads all the data from the file
 * and returns the data
 * @returns {object}
 */
export let getFileData = () => {
    try{
        let data = fs.readFile(pathToDataFile+file);
        return JSON.parse(data);
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let writeDataToFile = (data) => {
    try{
        fs.writeFile(pathToDataFile+file,JSON.stringify(data));
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

//--------------------------Store data -----------------------------//

export let storeUserData = (userObj,fileData) => {
    let data = {
        type :'user',
        data: {...userObj}
    }
    try{
        fileData = FindAndRemoveObject("user",fileData);
        fileData.data.push(data);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let storeBirdData = (birdObj,fileData) => {
    try{
        fileData = FindAndRemoveObject("bird",fileData);
        fileData.data.push(birdObj);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let storeReserveData = (reserveObj,fileData) => {
    try{
        fileData = FindAndRemoveObject("reserve",fileData);
        fileData.data.push(reserveObj);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let storeSessionData = (sessionObj,fileData) => {
    try{
        fileData = FindAndRemoveObject("session",fileData);
        fileData.data.push(sessionObj);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let storeViolationData = (violationObj,fileData) => {
    try{
        fileData = FindAndRemoveObject("violation",fileData);
        fileData.data.push(violationObj);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

export let storeHuntedData = (huntedObj,fileData) => {
    try{
        fileData = FindAndRemoveObject("hunted",fileData);
        fileData.data.push(huntedObj);
        fs.writeFileSync(pathToDataFile+file,fileData);
        return true;
    }catch(err){
        console.error(err);
        throw err;
    }
}

//-----------------------Get info-----------------------//

export let getUserData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "user") return element;
    })
}

export let getReserveData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "reserve") return element;
    })
}

export let getSessionData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "session") return element;
    })
}

export let getBirdData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "bird") return element;
    })
}

export let getHuntedData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "hunted") return element;
    })
}

export let getVoilationData = () => {
    let data = getFileData();
    data.data.forEach(element=>{
        if(element.type === "violation") return element;
    })
}