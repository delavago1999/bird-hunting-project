import API from '../../api/config';

/**
 * Api fiunction for sign up to on the server,
 * first parameter is a object with user info
 * and second paramerter is the signup success function
 * @param {Object} personObj 
 * @param {function} signupSuccess 
 */
export let signup = async (personObj)=> {
    try{
        let data  = await API.post('/users/hunter/signup',{
            fname: personObj.fname,
            lname: personObj.lname,
            contact_number: personObj.contact_number,
            email: personObj.email,
            password: personObj.password
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                // "Access-Control-Allow-Origin": "*"
            }
        });
        if(data.data.success === true){
            alert('Signup Successful')
            return true;
        }
        return false;
    }catch(err){
        console.error(err);
    }
}

/**
 * 
 * @param {*} personObj 
 * @param {*} loginSuccess 
 */
export let login = async(personObj)=>{
    console.log("Hunter login")
    try{
        let data = await API.post('/users/hunter/login',{
            email: personObj.email,
            password: personObj.password
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                // "Access-Control-Allow-Origin": "*"
            }
        });
        
        if(data.data.success === true){
            console.log("Login Successful");
            return data.data.data;
        }
        return false;
    }catch(err){
        console.log("Login failed");
        alert("Login failed");
    }
}

/**
 * 
 * @param {*} personObj 
 * @param {*} loginSuccess 
 */
export let adminLogin = async(personObj)=>{
    console.log("Admin login")
    try{
        let data = await API.post('/users/admin/login',{
            email: personObj.email,
            password: personObj.password
        },{
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                // "Access-Control-Allow-Origin": "*"
            }
        });
        
        if(data.data.success === true){
            console.log("Login Successful");
            return data.data.data;
        }
        return false;
    }catch(err){
        console.log("Login failed");
        alert("Login failed");
    }
}