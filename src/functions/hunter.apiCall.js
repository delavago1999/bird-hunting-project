import API from '../api/config';

export let getAllHunters = async () => {
    try{
        let data =  await API.get('/users/hunter/',{},
        {
            headers: {
                // Accept: 'application/json',
                'Content-Type': 'application/json',
                // Authorization: 'Bearer ' + token,
            }
        });
        if(data.data.success===true){
            return data.data.data;
        }
        return false;
    }catch(err){
        console.error(err);
        throw err;
    }
}