import axios from 'axios';

export default axios.create({
    baseURL: 'https://bird-hunting-api.herokuapp.com',
    // baseURL: 'http://192.168.100.77:4000',
    // timeout: 1000,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
    },
  });
