import React from 'react';
import './main.css';

import UserInfoComponent from './componets/user.component';
import HuntingStats from './componets/huntingStats.component';
import HuntingSessionComponent from './componets/huntingSession.component';
import {connect} from 'react-redux';
class main extends React.Component{

    constructor(props){
        super(props);
        this.state= {
            loaded: "profile"
        }
    }

    componentDidMount(){
    }

    render(){
        return(
            <div id="main-container">

                <div id="left-bar">
                    <div id="left-top-wrapper">
                        <div id="left-bar-user-info">
                            <div id="user-icon-wrapper">
                                <img src={require('../images/user-icon.png')} alt="User Icon"/>
                            </div>
                            <p id="left-bar-user-name">Test</p>
                            <p id="left-bar-user-email">Test</p>
                        </div>

                        <div id="left-link-wrapper">
                            <ul>
                                <li id="bar-link-wrapper"><a href
                                onClick={()=>this.setState({loaded: 'profile'})}>Profile</a></li>
                                <li id="bar-link-wrapper"><a href
                                onClick={()=>this.setState({loaded: 'stats'})}>Hunting Statistics</a></li>
                                <li id="bar-link-wrapper"><a href
                                onClick={()=>this.setState({loaded: 'session'})}>Hunting Session</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div id="right-top">
                    <div id="user-icon-wrapper">
                        <img src={require('../images/user-icon.png')} alt="User Icon"/>
                    </div>
                </div>

                <div id="right-bottom">
                    {this.state.loaded==='profile'
                    ? <UserInfoComponent
                    editState={this.props.editState}
                    totalState={this.props.totalState}
                    /> 
                    : this.state.loaded==='stats' 
                    ? <HuntingStats
                    editState={this.props.editState}
                    totalState={this.props.totalState}
                    />
                    : this.state.loaded==='session'
                    ? <HuntingSessionComponent
                    editState={this.props.editState}
                    totalState={this.props.totalState}
                    />
                    : null}
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    let {reserve} = state;
    return {
      reserve: reserve,
    };
  };
  
  
  export default connect(mapStateToProps, null)(main);