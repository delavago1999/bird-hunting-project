import React from 'react';
import './loginStyle.css'
import UserIcon from '../../images/user-icon.png';
import {signup,login,adminLogin} from '../../functions/auth/auth.function';
import {connect} from 'react-redux';
import userAction from '../../redux/actions/user.action';
import store from '../../redux/store/store.redux';
// import {decodeToken} from '../../functions/localData.function';

class LoginScreen extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            loaded: 'Login',
            signupSuccess: false,
            loginSuccess: false,
            fname: "",
            lname: "",
            contact_number: "",
            email: "",
            password: "",
            input: null
        };
    }

    setFname = (val) =>{
        this.setState({fname: val});
    }

    setLname = (val) => {
        this.setState({lname: val})
    }

    setNumber = (val) => {
        this.setState({contact_number: val})
    }

    setEmail = (val) => {
        this.setState({email: val})
    }

    setInput = (val) => {
        this.setState({input: val})
    }

    setPassword = (val) => {
        this.setState({password: val})
    }

    loginSuccess = (val) => {
        this.setState({loginSuccess: val})
    }

    signupSuccess = (val) => {
        this.setState({signupSuccess: val});
        if(this.state.signupSuccess===true){
            this.setState({loaded: 'Login'});
        }
    }

    loadRegisterForm = () => {
        this.setState({loaded: 'Register'})
    }

    loadLoginForm = () => {
        this.setState({loaded: 'Login'})
    }

    register = () => {
        let person = {
            fname: this.state.fname,
            lname: this.state.lname,
            contact_number: this.state.contact_number,
            email: this.state.email,
            password: this.state.password
        }
        if(signup(person)){
            this.signupSuccess(true)
        }else{
            alert("Failed to create account")
        }
    }

    login = async() =>{
        let person = {
            email: this.state.email,
            password: this.state.password
        };

        let data = null;

        if(this.state.email === "admin@test.com"){
            data = await adminLogin(person);
        }else{
            data = await login(person);
        }
        
        if(data!==false){
            this.props.login({
                token: data.token,
                user: {...data.user.user}
            });
            console.log("user logged in!");
            console.log(store.getState());
            // this.props.editState('user',{
            //     token: data.token,
            //     user: {...data.user.user}
            // })
            console.log(this.props.totalState);
            this.props.loadScreen('main');
        }else{
            alert('Login Failed, try again');
        }
    }


    render(){
        
        return(
            <div id="main-wrapper">

                {/* <div id="background-style-container">

                </div> */}
                <div id="left-container">
                    <div id="left-text-wrapper">
                        <h1 id="text1">Welcome to B.I.R.D</h1>
                        <p id="text2">Register and get your hunting permit today</p>
                        {this.state.loaded === 'Login'? <p id="text3">Register <button type="button" onClick={()=>this.loadRegisterForm()}>Here</button></p> : <p id="text3">Login <button type="button" onClick={()=>this.loadLoginForm()}>Here</button></p>}
                    </div>
                </div>

                <div id="right-container">
                    {this.state.loaded === 'Login' ?
                     <Login 
                        loadScreen={this.props.loadScreen}
                        setPassword={this.setPassword}
                        setEmail={this.setEmail}
                        password={this.state.password}
                        email={this.state.email}
                        login={this.login}
                     /> 
                     : <Signup
                        setFname={this.setFname}
                        setLname={this.setLname}
                        setEmail={this.setEmail}
                        setNumber={this.setNumber}
                        setPassword={this.setPassword}
                        signup={this.register}
                      />}
                </div>

                {/* <footer></footer> */}
            </div>

        );
    }

}


let Login = (props) => {
    return(
        <div id="form-wrapper-main">

            <div id="section-header-container">
                <div id="user-icon-container">
                    <img src={UserIcon} alt="User Icon"/>
                </div>
                <p id="section-header">Login Here!</p>
            </div>

            <form>
                <div id="input_field_wrapper">
                    <input type="text" placeholder="Email *" name="email" id="input-fields" value={props.email} onChange={(val)=>props.setEmail(val.target.value)}/>
                </div>
                <div id="input_field_wrapper">
                    <input type="text" placeholder="Password *" name="password" id="input-fields" value={props.password} onChange={(val)=>props.setPassword(val.target.value)}/>
                </div>
            </form>

            <div id="btn-wrapper" onClick={()=>props.login()}><p id="btn-text">LOGIN</p></div>
        </div>
    )
}

let Signup = (props) => {
    return(
        <div id="form-wrapper-main" >

            <div id="section-header-container">
                <div id="user-icon-container">
                    <img src={UserIcon} alt="User Icon"/>
                </div>
                <p id="section-header">Register Here!</p>
            </div>

            <form>
                <div id="input_field_wrapper">
                    <input type="email" placeholder="Email/AdminId *" name="email" id="input-fields" onChange={(val)=>props.setEmail(val.target.value)}/>
                </div>
                <div id="input_field_wrapper">
                    <input type="text" placeholder="First Name *" name="f_name" id="input-fields" onChange={(val)=>props.setFname(val.target.value)}/>
                </div>
                <div id="input_field_wrapper">
                    <input type="text" placeholder="Last Name *" name="l_name" id="input-fields" onChange={(val)=>props.setLname(val.target.value)}/>
                </div>
                <div id="input_field_wrapper">
                    <input type="tel" placeholder="Phone Number *" name="number" id="input-fields" onChange={(val)=>props.setNumber(val.target.value)}/>
                </div>
                <div id="input_field_wrapper">
                    <input type="password" placeholder="Password *" name="password" id="input-fields" onChange={(val)=>props.setPassword(val.target.value)}/>
                </div>
            </form>

            <div id="btn-wrapper" onClick={()=>props.signup()}><p id="btn-text">SIGNUP</p></div>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
      login: (userObj) => {
        dispatch(userAction.login(userObj));
      }
    };
  };
  
export default connect(null, mapDispatchToProps)(LoginScreen);