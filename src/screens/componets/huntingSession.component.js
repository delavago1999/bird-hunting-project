import React from 'react';
import './huntingSession.component.style.css';
import {submitSession,getAllSessions} from '../../functions/hunting_session.api';
import {getAllReserves} from '../../functions/hunting_reserve.apiCalls';
import {connect} from 'react-redux';
import reserveAction from '../../redux/actions/reserve.action';
import birdAction from '../../redux/actions/bird.action';
import sessionAction from '../../redux/actions/session.action';
import {getAllBird} from '../../functions/bird.apicall';
import {submitHunted} from '../../functions/hunted.api';
import store from '../../redux/store/store.redux';

class HuntingSessionComponent extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            reserve: '',
            session: '',
            date: new Date(),
            bird: '',
            hunter: null,
            amount: 0,
            tableData: [],
            sessionObj: null
        }
    }
    
    setReserve = (val)=> {
        this.setState({reserve: val})
    }

    setSession = (val)=> {
        this.setState({session: val})
    }

    setDate = (val)=> {
        this.setState({date: val})
    }

    setBird = (val)=> {
        this.setState({bird: val})
    }

    setAmount = (val)=> {
        this.setState({amount: val})
    }

    setHunter = (val) => {
        this.setState({hunter: val})
    }

    findBirdById = (id) => {
        let bird = null;
        this.props.bird.forEach(element => {
            // console.log(element)
            if(element.id == id){
                bird= element;
            }
        });
        return bird;
    }

    submitBird = async() => {
        console.log(this.state.sessionObj)
        let rowData = {
            bird_name: this.findBirdById(this.state.bird).bird_name,
            amount: this.state.amount,
            type: this.findBirdById(this.state.bird).type
        }

        try{
            let data = await submitHunted({
                sessionId: this.state.sessionObj.id,
                birdId: parseInt(this.state.bird),
                amount:parseInt( this.state.amount)
            },this.props.user.token);
            console.log("Submited bird to database :D");
            console.log("server response: ", data);
        }catch(err){
            console.error(err);
        }

        let tempArr = this.state.tableData;
        tempArr.push(rowData);
        this.setState({tableData: tempArr})
        console.log("new Bird hunting table data!")
        console.log(this.state.tableData)
    }

    clickSubmit = async() => {

        console.log("Submitting Hunting Session to DATABASE")
        try{
            console.log(this.state.session);
            let data 
            if(this.props.user.user.type!=='admin'){
               data = await submitSession({
                    reserveId: this.state.reserve,
                    session: this.state.session,
                    date: this.state.date,
                    hunterId: this.props.user.user.id
                },this.props.user.token);
            }else{
                data = await submitSession({
                    reserveId: this.state.reserve,
                    session: this.state.session,
                    date: this.state.date,
                    hunterId: parseInt(this.state.hunter)
                },this.props.user.token);
            }
            this.setState({sessionObj: data});
            this.props.addSession(data);
            console.log(store.getState());
        }catch(err){
            console.log(err);
        }
    }

    async componentDidMount(){
        try{
            let data = await getAllReserves();
            if(data!==false){
                console.log("Storing reserves from database");
                this.props.addReserve(data);
                console.log(this.props.reserve);
            }
            let bird = await getAllBird();
            if(bird!==false){
                console.log("Storing birds from database");
                this.props.addBird(bird);
                console.log(this.props.bird)
                console.log(store.getState())
            }
        }catch(err){
            console.log(err);
        }
    }

    render(){

        return(
            <div id="hunting-session-component">
                <h2>Hunting Session</h2>
                <div id="tables-container">
                    <div id="tables-container-top">
                        {typeof this.props.reserve[0]!=='undefined'
                        ? <HuntingSessionSetter
                        reserve={this.props.reserve}
                        setReserve={this.setReserve}
                        setSession={this.setSession}
                        setDate={this.setDate}
                        clickSubmit={this.clickSubmit}
                        sessionObj={this.state.sessionObj}
                        hunters = {this.props.hunters}
                        setHunter={this.setHunter}
                        userType={this.props.user.user.type}
                        /> : null}

                        {typeof this.props.bird[0]!=='undefined'
                        ? <BirdSetter
                        bird={this.props.bird}
                        setBird={this.setBird}
                        setAmount={this.setAmount}
                        submitHuntedBird={this.submitBird}
                        /> : null}
                        
                    </div>
                    <div id="tables-container-bottom">
                        <BirdHuntedTable
                        tableData={this.state.tableData}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

let HuntingSessionSetter= (props)=> {

    return(
        <div id="hunting-session-card">
             <div id="header">
                <p>Hunting Session/Reserve</p>
            </div>
            <div class="session-top-card-body-wrappers">
                <select id="hunting-session-sectionlist" onChange={(val)=>props.setReserve(val.target.value)}>
                    {props.reserve.map(val => <option key={val.id} value={val.id}>{val.location}</option>)}
                </select>
                
                <select id="hunting-session-sectionlist" onChange={(val)=>props.setSession(val.target.value)}>
                    <option value="Day">Day</option>
                    <option value="Evening">Evening</option>
                </select>

                {
                    props.userType === 'admin'
                    ?   <select id="hunting-session-sectionlist" onChange={(val)=>props.setHunter(val.target.value)}>
                            {props.hunters.map(val=><option key={val.id} value={val.id}>{val.email}</option>)}
                        </select>
                    : null
                }

                <div id="session_input_field_wrapper">
                    <input type="date" placeholder="Date *" name="email" id="session_input-fields" value={props.email} onChange={(val)=>props.setDate(val.target.value)} />
                </div>

                <div id="session_submit-button" onClick={()=>props.sessionObj===null ? props.clickSubmit() : null} style={props.sessionObj==null ? null : {backgroundColor: '#7575'}}>
                    <p>Set Session</p>
                </div>
            </div>
        </div>
    );
}

let BirdSetter= (props)=> {
    return(
        <div id="hunting-session-card">
             <div id="header">
                <p>Bird</p>
            </div>
            <div class="session-top-card-body-wrappers">

                <select id="hunting-session-sectionlist" onChange={(val)=>props.setBird(val.target.value)}>
                    {props.bird.map(val=><option key={val.id} value={val.id}>{val.bird_name}</option>)}
                </select>
                
                <div id="session_input_field_wrapper">
                    <input type="number" placeholder="Amount *" name="email" id="session_input-fields" onChange={(val)=>props.setAmount(val.target.value)}/>
                </div>

                <div id="session_submit-button" onClick={()=>props.submitHuntedBird()}>
                    <p>Submit</p>
                </div>
            </div>
        </div>
    );
}

let BirdHuntedTable = (props) => {
    return(
        <div id="bird-table">
            <div id="header">
                <p>Birds Hunted</p>
            </div>
            <div id="table-body">
                <BirdHuntedRowData
                birdName="Bird Name"
                amount="Amount"
                type="Type"
                />
                {props.tableData.length>0 
                ? props.tableData.map(row=><BirdHuntedRowData birdName={row.bird_name} amount={row.amount} type={row.type}/>)
                : null}
                {/* // <BirdHuntedRowData/>
                // <BirdHuntedRowData/>
                // <BirdHuntedRowData/> */}
            </div>
        </div>
    );
}

let BirdHuntedRowData = (props) => {
    return(
        <div id="bird-table-row">
            <div>
                <p>{props.birdName}</p>
            </div>
            <div id="center-cell">
                <p>{props.amount}</p>
            </div>
            <div>
                <p>{props.type}</p>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    let {reserve,bird,user,hunters} = state;
    return {
      reserve: reserve,
      bird: bird,
      user: user,
      hunters: hunters
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      addReserve: (reserveObj) => {
        dispatch(reserveAction.addReserve(reserveObj));
      },
      addBird: (birdObj) => {
          dispatch(birdAction.addBird(birdObj))
      },
      addSession: (sessionObj) => {
        dispatch(sessionAction.addSession(sessionObj))
      }
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(HuntingSessionComponent);