import React from 'react';
import './huntingStats.component.style.css';
import { getAllHunted, getHuntedForUser } from '../../functions/hunted.api';
import { getAllSessions,getAllSessionsForUser } from '../../functions/hunting_session.api';
import { getAllViolation } from '../../functions/violation.apiCall';
import { getAllReserves } from '../../functions/hunting_reserve.apiCalls';
import store from '../../redux/store/store.redux';
import reserveAction from '../../redux/actions/reserve.action';
import birdAction from '../../redux/actions/bird.action';
import sessionAction from '../../redux/actions/session.action';
import violationAction from '../../redux/actions/violation.action'
import huntedAction from '../../redux/actions/hunted.action';
import {connect} from 'react-redux';
import { getAllBird } from '../../functions/bird.apicall';

class HuntingStats extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            violations: null,
            sessions: null,
            hunts: null,
            birds:[],
            adminData: null
        }
    }

    async componentDidMount(){
        try{
            let data = await getAllBird();
            if(data!==false){
                this.props.addBird(data);
            }
            data = await getAllHunted();
            if(data!==false){
                this.props.addHunted(data);
            }
            data = await getAllSessions(this.props.user.token);
            if(data!==false){
                this.props.addSession(data);
            }
            data = await getAllViolation(this.props.user.token);
            if(data!==false){
                this.props.addViolation(data);
            }
            data = await getAllReserves(this.props.user.token);
            if(data!=false){
                this.props.addReserve(data);
            }
            console.log("got all birds state and hunted")
            console.log(store.getState());

        }catch(err){
            console.error(err);
        }

        this.calculations();
        if (this.props.user.user.type === 'admin'){
            this.setState({adminData: this.createBigHunterObjects()});
            this.setState({violations:  this.calculateAdminViolationData()});
        }
    }

    getSessionIds = (sessinArray) => {
        let temp = [];
        sessinArray.forEach(element=> {
            temp.push(element.id);
        })

        return temp;
    }

    calculateAdminViolationData = () => {
        let checkIds = []
        let violationArr = []

        this.state.adminData.forEach(element=> {
            let email = element.email;
            let bag = 0;
            let wcp = 0;
            let protectedBird = 0;
            this.state.adminData.forEach(index=> {
                if(checkIds.find((val)=>index.email===val)!==undefined && index.email === element.email){
                    if(index.type==='protected') protectedBird+=1;
                    if(index.bird_name==='White-crowned Pigeon')wcp+=1;
                    bag+=1;
                }
            })
            if(bag>20){
                violationArr.push({email: element.email ,...this.props.violation.voilations[0]});
            }
            if(wcp>30){
                violationArr.push({email: element.email ,...this.props.violation.voilations[1]});
            }
            if(protectedBird>0){
                violationArr.push({email: element.email, ...this.props.violation.voilations[2]});
            }

            checkIds.push(element.email);
        })

        return violationArr;
    }

    createBigHunterObjects = () => {
        let newArr = [];
        this.props.hunters.forEach(hunter=> {
            this.props.session.forEach(session=> {
                this.props.hunted.forEach(hunt=> {
                    this.props.bird.forEach(bird=> {
                        this.props.reserve.forEach(reserve=> {
                            if(hunter.id === session.hunterId && session.id === hunt.sessionId && session.reserveId === reserve.id && hunt.birdId === bird.id){
                                newArr.push({
                                    ...hunter,
                                    ...session,
                                    ...hunt,
                                    ...reserve,
                                    ...bird
                                })
                            }
                        })
                    })
                })
            })
        })
        console.log(newArr)
        return newArr;
    }

    calculations = () => {
        let sessionTemp = [];
        this.props.session.forEach(element => {
            if(element.hunterId===this.props.user.user.id){
                sessionTemp.push(element);
            }
        });
        let tempHunts = [];
        this.props.hunted.forEach(element=> {
            for(let i = 0; i<sessionTemp.length; i++){
                if(element.sessionId===sessionTemp[i].id){
                    tempHunts.push(element);
                }
            }
        })

        let bag = 0;
        let wcp = 0;
        let protectedBird = 0;
        let tempViolations = [];

        sessionTemp.forEach(element=> {
            tempHunts.forEach(val=> {
                if(val.sessionId === element.id){
                    bag+=val.amount;
                    this.props.bird.forEach(index=> {
                        if(index.id===val.birdId){
                            if(index.type==='protected') protectedBird+=1;
                            if(index.bird_name==='White-crowned Pigeon')wcp+=1;
                        }
                    })
                }
            })

            if(bag>20){
                tempViolations.push(this.props.violation.voilations[0]);
            }

            if(wcp>30){
                tempViolations.push(this.props.violation.voilations[1]);
            }

            if(protectedBird>0){
                tempViolations.push(this.props.violation.voilations[2]);
            }
            this.setState({violations: tempViolations});
            this.setState({hunts: tempHunts});
            this.setState({sessions: tempViolations});
            console.log(tempViolations,tempHunts,sessionTemp);
        })

        // this.birdCalc();
    }

    birdCalc = () => {
        let temp = [], i=0;
        temp = this.state.hunts;
        this.state.hunts.forEach(element => {
            if(element.birdId===temp[i].birdId){
                temp[i].amount+=element.amount
                temp.splice(i,1);
            }
            i++;
        });

        console.log(temp);
    }

    render(){
        return(
            <div id="hunting-stats-component">
                <h2>Hunting Statistics</h2>
                <div id="stats-tables-container">
                    {this.state.hunted!==null && this.props.user.user.type === 'hunter'
                    ? <BirdHuntedTable
                    hunted={this.state.hunts}
                    birds={this.props.bird}
                    />
                    : null}
                    {this.state.violations!==null && this.props.user.user.type === 'hunter'
                    ? <ViolationsTable
                    violations={this.state.violations}
                    />
                    : null}

                    {
                        this.props.user.user.type === 'admin' && this.state.adminData!==null? 
                        <AdminBirdHuntedTable data={this.state.adminData}/>
                        :null
                    }

                    {
                        this.props.user.user.type === 'admin' && this.state.violations!==null? 
                        <AdminViolationsTable violations={this.state.violations}/> : null
                    }
                </div>
            </div>
        );
    }
}

let AdminBirdHuntedTable = (props) => {
    return(
        <div id="bird-table">
            <div id="header">
                <p>Birds Hunted</p>
            </div>
            <div id="table-body">
                <AdminBirdHuntedRowData
                    hunter="Hunter"
                    session="Session"
                    reserve="Reserve"
                    bird="Bird"
                    type="Bird Type"
                    amount="Hunted"
                />
                { 
                    props.data.map(val=> <AdminBirdHuntedRowData
                        hunter={val.email}
                        session={val.session}
                        reserve={val.location}
                        bird={val.bird_name}
                        type={val.type}
                        amount={val.amount}
                    />)
                }
                
            </div>
        </div>
    );
}

let BirdHuntedTable = (props) => {
    return(
        <div id="bird-table">
            <div id="header">
                <p>Birds Hunted</p>
            </div>
            <div id="table-body">
                <BirdHuntedRowData
                bird_name="Bird Name"
                hunted="Amount Hunted"
                type="Type"
                />
                {
                    props.hunted !== null 
                    ? props.hunted.map(val=> <BirdHuntedRowData
                        bird_name={props.birds[val.birdId-1].bird_name}
                        hunted={val.amount}
                        type={props.birds[val.birdId-1].type}
                        />)
                    : null
                }
                
                <BirdHuntedRowData/>
                <BirdHuntedRowData/>
            </div>
        </div>
    );
}

let BirdHuntedRowData = (props) => {
    return(
        <div id="bird-table-row">
            <div>
                <p>{props.bird_name}</p>
            </div>
            <div id="center-cell">
                <p>{props.hunted}</p>
            </div>
            <div>
                <p>{props.type}</p>
            </div>
            
        </div>
    )
}

let AdminBirdHuntedRowData = (props) => {
    return(
        <div id="admin-bird-table-row">
            <div>
                <p>{props.hunter}</p>
            </div>
            <div id="center-cell">
                <p>{props.session}</p>
            </div>
            <div>
                <p>{props.reserve}</p>
            </div>
            <div>
                <p>{props.bird}</p>
            </div>
            <div>
                <p>{props.type}</p>
            </div>
            <div>
                <p>{props.amount}</p>
            </div>
            
        </div>
    )
}

let AdminViolationsTable = (props) => {
    return(
        <div id="violation-table">
            <div id="header">
                <p>Violations</p>
            </div>
            <div id="table-body">
                <AdminViolationRowData
                hunter="Hunter"
                violation="Violation"
                description="Description"
                />
                {props.violations.map(val=> <AdminViolationRowData violation={val.type} description={val.description} hunter={val.email} key={val.id}/>)}
            </div>
        </div>
    );
}

let ViolationsTable = (props) => {
    return(
        <div id="violation-table">
            <div id="header">
                <p>Violations</p>
            </div>
            <div id="table-body">
                <ViolationRowData
                violation="Violation"
                description="Description"
                />
                {props.violations.map(val=> <ViolationRowData violation={val.type} description={val.description} key={val.id}/>)}
            </div>
        </div>
    );
}

let AdminViolationRowData = (props) => {
    return(
        <div id="admin-violation-table-row">
            <div id="first-cell">
                <p>{props.violation}</p>
            </div>
            <div>
                <p>{props.hunter}</p>
            </div>
            <div>
                <p>{props.description}</p>
            </div>
        </div>
    )
}

let ViolationRowData = (props) => {
    return(
        <div id="violation-table-row">
            <div id="first-cell">
                <p>{props.violation}</p>
            </div>
            <div>
                <p>{props.description}</p>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    let {reserve,bird,user,hunted,session,violation,hunters} = state;
    return {
      reserve: reserve,
      bird: bird,
      user: user,
      hunted: hunted,
      session: session,
      violation: violation,
      hunters: hunters
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      addReserve: (reserveObj) => {
        dispatch(reserveAction.addReserve(reserveObj));
      },
      addBird: (birdObj) => {
          dispatch(birdAction.addBird(birdObj))
      },
      addSession: (sessionObj) => {
        dispatch(sessionAction.addSession(sessionObj))
      },
      addHunted: (huntedObj) => {
        dispatch(huntedAction.addHunted(huntedObj))
      },
      addViolation: (violationObj) => {
          dispatch(violationAction.addViolation(violationObj))
      }
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(HuntingStats);