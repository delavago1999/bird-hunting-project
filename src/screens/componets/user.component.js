import React from 'react';
import {getAllHunters} from '../../functions/hunter.apiCall';
import hunterAction from '../../redux/actions/hunter.action';
import './user.component.style.css';
import {connect} from 'react-redux';
import store from '../../redux/store/store.redux';


class UserInfoComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    async componentDidMount(){
        if(this.props.user.user.type==='admin'){
            let data = await getAllHunters();
            if(data!==false){
                this.props.addHunter(data);
                console.log(store.getState());
            }
        }
    }

    render(){
        return(
            <div id="user-info-component">
                <UserInfo
                user={this.props.user}
                />
                <HunterCard
                user={this.props.user}
                />
            </div>
        );
    }
}

class UserInfo extends React.Component{
    // componentDidMount(){
    //     alert(JSON.stringify(this.props.totalState.user.user))
    // }
    render(){
        return(
            <div id="card-wrapper">
                <div id="card-header">
                    <p>User Information</p>
                </div>
                <div id="card-body">
                    <UserInfoRecord
                    label="First Name"
                    info={this.props.user.user.fname}
                    />
                    <UserInfoRecord
                    label="Last Name"
                    info={this.props.user.user.lname}
                    />
                    <UserInfoRecord
                    label="Email"
                    info={this.props.user.user.email}
                    />

                    <UserInfoRecord
                    label="Account Type"
                    info={this.props.user.user.type}
                    />
                </div>
            </div>
        );
    }
}

let UserInfoRecord = (props) => {
    return(
        <div id="card-info">
            <p id="info-type">{props.label}</p>
            <p id="info">{props.info}</p>
        </div>
    )
}

class HunterCard extends React.Component{
    render(){
        return(
            <div id="hunter-card">
                <div id="hunter-card-header">
                    <p>Name:</p>
                    <p>Hunter Name</p>
                </div>
                <div id="hunter-card-body">
                    <div id="hunter-id-wrapper">
                        <p>{this.props.user.user.fname.substring(0,3)}-{this.props.user.user.type.substring(0,3)}-00{this.props.user.user.id}</p>
                    </div>
                    <p id="expiry-date">Sept 20, 2019</p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    let {user} = state;

    return {
      user: user
    };
};

const mapDispatchToProps = dispatch => {
    return{
        addHunter: (hunterObj) => {
            dispatch(hunterAction.addHunter(hunterObj));
        }
    };
}
  
export default connect(mapStateToProps, mapDispatchToProps)(UserInfoComponent);