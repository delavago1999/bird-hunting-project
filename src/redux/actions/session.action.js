import ST from '../types/session.type';

let addSession = sessionObj => {
    return{
        type: ST.ADD_SESSION,
        payload: sessionObj
    }
}

export default {
    addSession
}