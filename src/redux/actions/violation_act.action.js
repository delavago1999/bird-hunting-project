import VAT from '../types/violation_action.type';

let addViolationAct = violationActObj => {
    return{
        type: VAT.ADD_VIOLATION_ACT,
        payload: violationActObj
    }
}

export default {
    addViolationAct
}