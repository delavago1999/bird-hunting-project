import HT from '../types/hunted.type';

let addHunted = huntedObj => {
    return{
        type: HT.ADD_HUNTED,
        payload: huntedObj
    }
}

export default {
    addHunted
}