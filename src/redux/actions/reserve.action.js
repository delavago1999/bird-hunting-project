import RT from '../types/reserve.type';

let addReserve = reserveObj => {
    return{
        type: RT.ADD_RESERVE,
        payload: reserveObj
    }
}

export default {
    addReserve
}