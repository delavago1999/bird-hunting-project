import UT from '../types/user.types';

let login = userObj => {
    return{
        type: UT.LOGIN,
        payload: userObj
    }
}

export default {
    login
}