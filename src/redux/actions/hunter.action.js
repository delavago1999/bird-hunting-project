import HT from '../types/hunter.type';

let addHunter = hunterObj => {
    return{
        type: HT.ADD_HUNTER,
        payload: hunterObj
    }
}

export default {
    addHunter
}