import VT from '../types/violation.type';

let addViolation = violationObj => {
    return{
        type: VT.ADD_VIOLATION,
        payload: violationObj
    }
}

export default {
    addViolation
}