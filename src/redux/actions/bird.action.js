import BT from '../types/bird.type';

let addBird = birdObj => {
    return{
        type: BT.ADD_BIRD,
        payload: birdObj
    }
}

export default {
    addBird
}