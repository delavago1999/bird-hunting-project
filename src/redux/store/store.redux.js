import { createStore, combineReducers } from 'redux';
import userReducer from '../reducers/user.reducer';
import reserveReducer from '../reducers/reserve.reducer';
import sessionReducer from '../reducers/session.reducer';
import birdReducer from '../reducers/bird.reducer';
import violationReducer from '../reducers/violation.reducer';
import violationActReducer from '../reducers/violationAct.reducer';
import huntedReducer from '../reducers/hunted.reducer';
import hunterReducer from '../reducers/hunter.reducer';

const rootReducer = combineReducers({
    user: userReducer,
    reserve: reserveReducer,
    session: sessionReducer,
    bird: birdReducer,
    violation: violationReducer,
    violationAct: violationActReducer,
    hunted: huntedReducer,
    hunters: hunterReducer
});

const store = createStore(rootReducer);

console.log("created redux store!")
console.log(store.getState());

export default store;