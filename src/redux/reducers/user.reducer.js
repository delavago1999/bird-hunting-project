import userTypes from "../types/user.types";

const initialState = {}

export default  (state = initialState, action) => {
    switch(action.type){
        case userTypes.LOGIN :
            return action.payload
        default: 
            return state;
    }
}