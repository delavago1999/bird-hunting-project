import sessionType from "../types/session.type";

const initialState = []

export default (state = initialState, action) => {
    switch(action.type){
        case sessionType.ADD_SESSION:
            return action.payload
        default:
            return state;
    }
}