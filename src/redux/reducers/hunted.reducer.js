import huntedType from "../types/hunted.type";

const initialState = {}

export default (state = initialState, action) => {
    switch(action.type){
        case huntedType.ADD_HUNTED:
            return action.payload
        default:
            return state;
    }
}