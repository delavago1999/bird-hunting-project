import violationType from "../types/violation.type";

const initialState = []

export default (state = initialState, action) => {
    switch(action.type){
        case violationType.ADD_VIOLATION:
            return action.payload
        default:
            return state;
    }
}