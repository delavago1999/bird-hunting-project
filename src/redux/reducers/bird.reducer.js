import birdType from "../types/bird.type";

const initialState = []

export default (state = initialState, action) => {
    switch(action.type){
        case birdType.ADD_BIRD:
            return action.payload
        default:
            return state;
    }
}