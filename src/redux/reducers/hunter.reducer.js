import hunterType from "../types/hunter.type";

const initialState = {}

export default (state = initialState, action) => {
    switch(action.type){
        case hunterType.ADD_HUNTER:
            return action.payload
        default:
            return state;
    }
}