import violationAct from "../types/violation_action.type";

const initialState = []

export default (state = initialState, action) => {
    switch(action.type){
        case violationAct.ADD_VIOLATION_ACT:
            return action.payload
        default:
            return state;
    }
}