import reserveType from "../types/reserve.type";

const initialState = {}

export default (state = initialState, action) => {
    switch(action.type){
        case reserveType.ADD_RESERVE:
            return action.payload
        default:
            return state;
    }
}