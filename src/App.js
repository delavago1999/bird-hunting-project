import React from 'react';
import './App.css';

import Login from './screens/authScreens/login.screen';
import Main from './screens/main.screen';

class App extends React.Component{

  constructor(props){
    super(props)
    this.state = {
        loaded:  'login',
        user: {},
        reserve: {},
        session: {},
        bird: {},
        hunted: {},
        violation: {},
        totalState:{}
      };
  }

  setUser = (userObj) => {
    this.setState({user: userObj});
  }

  setReserve = (reserveObj) => {
    this.setState({reserve: reserveObj});
  }

  setSession = (sessionObj) => {
    this.setState({session: sessionObj});
  }

  setBird = (birdObj) => {
    this.setState({bird: birdObj});
  }

  setHunted = (huntedObj) => {
    this.setState({hunted: huntedObj});
  }

  setViolation = (violationObj) => {
    this.setState({violation: violationObj});
  }

  setTotalState = (type,obj) => {
    console.log("setting state! type: ",type);
    switch(type){
      case 'user':
        console.log("setting user")
        this.setUser(obj);
        break;
      case 'reserve': 
        this.setReserve(obj);
        break;
      case 'session':
          this.setSession(obj);
          break;
      case 'hunted': 
        this.setHunted(obj);
        break;
      case 'violation': 
        this.setViolation(obj);
        break;
      case 'bird': 
        this.setBird(obj);
        break;
      default: 
        console.log('The type you passed to setTotalState is invalid')
        alert('The type you passed to setTotalState is invalid')
        break;
    }

    this.setState({totalState: 
      {user: this.state.user,
      reserve: this.state.reserve,
      session: this.state.session,
      hunted: this.state.hunted,
      violation: this.state.violation,
      bird: this.state.bird}
    })
  }


  loadScreen = (string) => {
      this.setState({loaded: string})
  };

  componentDidMount(){
    this.setState({totalState: 
      {user: this.state.user,
      reserve: this.state.reserve,
      session: this.state.session,
      hunted: this.state.hunted,
      violation: this.state.violation,
      bird: this.state.bird}
    })
  }

  render(){
    return(
      <div>
          {this.state.loaded === 'login' 
          ? <Login loadScreen={this.loadScreen}
          editState={this.setTotalState}
          totalState={this.state.totalState}
          /> 
          : <Main
          editState={this.setTotalState}
          totalState={this.state.totalState}
          />
          }
      </div>
    )
  }
}

export default App;
