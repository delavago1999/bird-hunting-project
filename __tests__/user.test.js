let req = require('supertest');
let bcrypt = require('bcryptjs');
let {BASE_URL} = require('../testConfig/test.config');


console.log(BASE_URL);
/**
 * password hashing function
 * @param password
 * @returns {Promise<any>} hashedPassword
 */
let passwordHasher = (password) => {
    return new Promise((resolve, reject)=>{
        bcrypt.genSalt(10,(err,salt)=>{
            if(err) return reject(password);
            bcrypt.hash(password, salt,(err,hash)=>{
                if(err) return reject(password);
                return resolve(hash);
            })
        })
    })
};

/**
 * Success tests
 */
describe('User API requests => success', ()=> {
    let res;
    let userId;
    //login test
    it('Login: should return a token and sucess true', async() => {
        res = await req(BASE_URL)
        .post('/users/login')
        .send({
            "user_name": "user1",
            "user_password": "user1"
        })
        .expect(200);//change to 200
        expect(typeof res.body.token).toBe('string')
        expect(res.body.success).toBe(true)
    },30000);

    // //signup test
    // it('Signup: should respond with a success true status', async()=> {
    //     res = await req(BASE_URL)
    //     .post('/users/signup')
    //     .send({
    //         "user_name": "newUser",
    //         "user_email": "newUser@gmail.com",
    //         "user_password": await passwordHasher('newUser')
    //     })
    //     .expect(201);
    //     expect(res.body.success).toBeTruthy()
    //     expect(res.body.data).toBe(null);
    //     expect(res.body.error).toBe(null); 
    // },30000)
})

/**
 * Failure tests
 */
describe('User API requests => Failure', () => {
    it('Login: should return failure and user not found', async() => {
        res = await req(BASE_URL)
        .post('/users/login')
        .send({
            "user_name": "unknowUser",
            "user_password": "unknowUser"
        })
        .expect(200);
        expect(res.body.success).toBeFalsy();
        expect(res.body.message).toBe('User not found');
        expect(res.body.status.code).toBe('0010');
    },3000);

    it('Login: should return password not match error', async()=> {
        res = await req(BASE_URL)
        .post('/users/login')
        .send({
            "user_name": "user1",
            "user_password": "unknowUser"
        })
        .expect(200);
        expect(res.body.message).toBe('Password does not match');
        expect(res.body.status.code).toBe('0004');
    })
})