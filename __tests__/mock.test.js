const sequelizeConfig = require('../config/config.json');
const dotenv = require('dotenv').config();
let {BASE_URL} = require('../testConfig/test.config');


// let BASE_URL = process.env.SETTINGS =='development' ? 'http://localhost:3000' : 'https://test-progech.herokuapp.com';

console.log(dotenv.parsed.SETTINGS);

test('environment should be set to test', () => {
    console.log(process.env.NODE_ENV);
    expect(process.env.NODE_ENV).toBe('test')
})

test('sequelize config should be in test mode', () => {
    console.log(sequelizeConfig.test.username);
    expect(sequelizeConfig.test.username).toBe('zoVr25A2DJ');
})
